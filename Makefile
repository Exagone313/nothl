# Copyright (C) 2018, Elouan Martinet <exa@elou.world>
# All rights reserved.
#
# This file is part of Not Hard Linked.
#
# Not Hard Linked is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What the Fuck You Want
# to Public License, Version 2, as published by Sam Hocevar. See
# <http://www.wtfpl.net/> for more details.

NAME ?= nothl

CFLAGS ?= -O3
CPPFLAGS ?= -Wall -Wextra -Werror

SRC := nothl.c

OBJ := $(SRC:.c=.o)

.PHONY: all
all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^

.PHONY: clean
clean:
	$(RM) $(OBJ)

.PHONY: fclean
fclean: clean
	$(RM) $(NAME)
