# Not Hard Linked

List regular files that have only one hard link.

## Usage

```
make
./nothl [dir [nested_directory_limit]]
```
