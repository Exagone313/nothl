/*
** Copyright (C) 2018, Elouan Martinet <exa@elou.world>
** All rights reserved.
**
** This file is part of Not Hard Linked.
**
** Not Hard Linked is free software. It comes without any warranty, to
** the extent permitted by applicable law. You can redistribute it
** and/or modify it under the terms of the Do What the Fuck You Want
** to Public License, Version 2, as published by Sam Hocevar. See
** <http://www.wtfpl.net/> for more details.
*/

#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define NESTED_MAX 5

int list_files(char *path, int nested)
{
	DIR *dir;
	struct dirent *ent;
	struct stat sb;
	char nested_path[PATH_MAX + 1] = {0};
	int i;

	if (nested <= 0)
		return 0;
	if (!(dir = opendir(path)))
	{
		fprintf(stderr, "Cannot open %s: %s\n", path,
				strerror(errno));
		return 1;
	}
	while ((ent = readdir(dir)))
	{
		if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0)
		{
			memcpy(nested_path, path, PATH_MAX);
			i = strlen(nested_path);
			strncpy(nested_path + i, "/", PATH_MAX - i);
			i = strlen(nested_path);
			strncpy(nested_path + i, ent->d_name, PATH_MAX - i);
			if (stat(nested_path, &sb) < 0)
			{
				fprintf(stderr, "Cannot stat %s: %s\n", nested_path,
						strerror(errno));
			}
			else
			{
				if ((sb.st_mode & S_IFMT) == S_IFREG && sb.st_nlink <= 1)
					printf("%s\n", nested_path);
				else if ((sb.st_mode & S_IFMT) == S_IFDIR)
					list_files(nested_path, nested - 1);
			}
		}
	}
	closedir(dir);
	return 0;
}

int main(int argc, char **argv)
{
	char *path;
	int nested;

	path = (argc > 1) ? argv[1] : ".";
	nested = (argc > 2) ? atoi(argv[2]) : NESTED_MAX;
	if (nested <= 0)
		nested = NESTED_MAX;
	return list_files(path, nested);
}
